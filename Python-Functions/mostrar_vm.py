from ArubaCloud.PyArubaAPI import CloudInterface
from ArubaCloud.objects.VmTypes import Smart


def mostrar_vm(user, passwd):
	ci = CloudInterface(dc=1)
	ci.login(username=user, password=passwd, load=True)
	for vm in ci.vmlist:
		print('Nombre: ' + vm.vm_name + ' Id: ' + str(vm.sid))			

if __name__ == '__main__':
	user = input("Ingresa tu usuario\n")
	passwd = input("Ingresa tu password\n")
	mostrar_vm(user, passwd)