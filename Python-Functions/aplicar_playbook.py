<<<<<<< HEAD
import os
import subprocess
from collections import namedtuple
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.executor.playbook_executor import PlaybookExecutor

def aplicar_playbook(user, host, port, playbookName):
    
    try:
    	keyscanCommand = "ssh-keyscan " +host+ " >> /"+user+"/.ssh/known_hosts" 
    	subprocess.call(keyscanCommand, shell=True)

    	loader = DataLoader()
    	    
    	inventory = InventoryManager(loader=loader, sources=['/var/www/html/hosts/hosts'])
    	variable_manager = VariableManager(loader=loader, inventory=inventory)
    	 
    	passwords={}
    	 
    	Options = namedtuple('Options',
    	                     ['connection',
    	                      'remote_user',
    	                      'ask_sudo_pass',
    	                      'verbosity',
    	                      'ack_pass', 
    	                      'module_path', 
    	                      'forks', 
    	                      'become', 
    	                      'become_method', 
    	                      'become_user', 
    	                      'check',
    	                      'listhosts', 
    	                      'listtasks', 
    	                      'listtags',
    	                      'syntax',
    	                      'sudo_user',
    	                      'sudo',
    	                      'diff',
    	                      'host_key_checking'])

    	options = Options(connection='smart', 
    	                       remote_user='root',
    	                       ack_pass=True,
    	                       sudo_user=None,
    	                       forks=100,
    	                       sudo=None,
    	                       ask_sudo_pass=True,
    	                       verbosity=5,
    	                       module_path=None,  
    	                       become=None, 
    	                       become_method='sudo', 
    	                       become_user='root', 
    	                       check=False,
    	                       diff=False,
    	                       listhosts=None,
    	                       listtasks=None, 
    	                       listtags=None, 
    	                       syntax=None,
    	                       host_key_checking = False)
    	 
    	playbook = PlaybookExecutor(playbooks=["/var/www/html/playbooks/"+playbookName+".yml"],inventory=inventory,
    	              variable_manager=variable_manager,
    	              loader=loader,options=options,passwords=passwords)
    	playbook.run()

    except Exception as e:
            print(str(e))

    finally:
    	reverseKeygen = "ssh-keygen -R " +host
    	subprocess.call(reverseKeygen, shell=True)

    

if __name__ == '__main__':
	user = input("Ingresa tu usuario\n")
    host = input("Ingresa tu host\n")
    port = input("Ingresa el puerto\n")
    playbookName = input("Ingresa el nombre del playbook\n")
    aplicar_playbook(user, host, port, playbookName)

=======
import requests
import tempfile
import os

def aplicar_playbook(namePlaybook):

	responsePb = requests.get('http://80.211.56.94/playbooks/' + namePlaybook + '.yml')

	responseHost = requests.get('http://80.211.56.94/hosts/hosts')

	hosts = open("/tmp/hosts", "w+")
	playbook = open("/tmp/playbook.yml", "w+")

	try:
		playbook.writelines(responsePb.text)
		playbook.seek(0)

		hosts.writelines(responseHost.text)
		hosts.seek(0)
	
		os.system('cd /tmp && ansible-playbook playbook.yml')
		
	finally:
		playbook.close()
		hosts.close()
		os.chdir('/tmp')
		os.remove('hosts')
		os.remove('playbook.yml')

if __name__ == '__main__':
	namePlaybook = input("Ingresa el nombre del playbook\n")
	aplicar_playbook(namePlaybook)
>>>>>>> 1107e957febf6aff5f97d465b30f35ab29c707c3
