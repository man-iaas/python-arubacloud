#!/usr/bin/env python
# -*- coding: utf-8
 
import os
from ftplib import FTP 

def mostrar_playbooks(host, usuario, contrasena):
	ftp = FTP() 
	ftp.connect(host)
	ftp.login(usuario, contrasena)
	ftp.cwd('/var/www/html/playbooks')
	print(ftp.nlst())

if __name__ == '__main__':
	host = input('Ingresa el host del servidor\n')
	usuario = input('Ingresa el usuario\n')
	contrasena = input('Ingresa la contrasena\n')
	mostrar_playbooks(host, usuario, contrasena) 
	
