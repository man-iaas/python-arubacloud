from ArubaCloud.PyArubaAPI import CloudInterface
from ArubaCloud.objects.VmTypes import Smart
import time

def restaurar(user, passwd, nombre, contra):
	ci = CloudInterface(dc=1)
	ci.login(username=user, password=passwd, load=True)
	for vm in ci.vmlist:
		if vm.vm_name == nombre:
			ci.poweroff_server(vm,vm.sid)
			print('Apagando Servidor...')
			time.sleep(20)

			vm.reinitialize(admin_password=contra)
			print('Restaurando Servidor...')


if __name__ == '__main__':
	user = input("Ingresa tu usuario\n")
	passwd = input("Ingresa tu password\n")
	nombre = input("Ingresa nombre del servidor\n")
	contra = input("Ingresa nueva contrasena para servidor\n")
	restaurar(user, passwd, nombre,contra)