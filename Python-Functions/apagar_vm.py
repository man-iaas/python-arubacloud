from ArubaCloud.PyArubaAPI import CloudInterface
from ArubaCloud.objects.VmTypes import Smart
import time

def apagar_servidor(user, passwd, nombre):
	ci = CloudInterface(dc=1)
	ci.login(username=user, password=passwd, load=True)
	for vm in ci.vmlist:
		if vm.vm_name == nombre:
			ci.poweroff_server(vm,vm.sid)
			print('Apagando Servidor...')
			time.sleep(20)


if __name__ == '__main__':
	user = input("Ingresa tu usuario\n")
	passwd = input("Ingresa tu password\n")
	nombre = input("Ingresa nombre del servidor\n")
	apagar_servidor(user, passwd, nombre)